<?php


class Shopware_Plugins_Frontend_ArvShowSubcategories_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{
    private $pluginInfo = array(
        'version' => '1.0.0',
        'label' => 'ArvShowSubcategories',
        'supplier' => 'arvatis media GmbH',
        'description' => 'Widget, um Artikel aus einer festzulegenden Kategorie in einer Einkaufswelt anzuzeigen.',
        'support' => 'auf Anfrage',
        'link' => 'http://www.arvatis.com'
    );

    public function getInfo()
    {
        return $this->pluginInfo;
    }


    public function getLabel()
    {
        return 'Zeigt von einer ausgewählten Kategorie die Subkategorien an';
    }

    public function getVersion()
    {
        return "1.0.0";
    }

    public function install()
    {
        $component = $this->createEmotionComponent(array(
            'name' => $this->getLabel(),
            'template' => 'emotion_subcategories',
            'description' => $this->pluginInfo['description']
        ));

        $component->createField(array(
            'name' => 'cat_id',
            'fieldLabel' => 'Kategorie ID',
            'allowBlank' => false,
            'xtype' => 'emotion-components-fields-category-selection'
        ));

        $component->createTextField(array(
            'name' => 'count_categories',
            'fieldLabel' => 'Anzahl Sub-Kategorien',
            'allowBlank' => false,
            'defaultValue' => 6
        ));

        $component->createHiddenField(array(
            'name' => 'emotion_subcats',
            'allowBlank' => true
        ));

        $this->subscribeEvent("Shopware_Controllers_Widgets_Emotion_AddElement", "onAddElement");

        return true;
    }

    public function uninstall()
    {
        return true;
    }

    public function onAddElement(Enlight_Event_EventArgs $arguments)
    {
        $data = $arguments->getReturn();

        if (isset($data['emotion_subcats'])) {
            $categoryId = (int)$data['cat_id'];
            $countCategories = (int)$data['count_categories'];

            $subCategories = Shopware()->Modules()->Categories()->sGetWholeCategoryTree($categoryId, 1);
            $subCategories = array_slice($subCategories, 0, $countCategories);
            $data['subCategories'] = $subCategories;
        }

        return $data;
    }
}
