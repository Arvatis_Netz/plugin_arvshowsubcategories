<div class="selectCategoryWrap">
    <label for="selectCategory">Produkte aus dem Monat:</label>
    <select name="selectCategory" id="selectArchiveMonth"{if $Data.subCategories[0].attribute.attribute6} style="color:{$Data.subCategories[0].attribute.attribute6}"{/if}>
        <optgroup>
            {foreach $Data.subCategories as $category}
                <option data-url="{$category.link}">{$category.name}</option>
            {/foreach}
        </optgroup>
    </select>
</div>